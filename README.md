# Plymouth themes for Manjaro

![AUR version](https://img.shields.io/aur/version/plymouth-theme-manjaro-charge)
![AUR last update](https://img.shields.io/aur/last-modified/plymouth-theme-manjaro-charge)
![AUR maintainer](https://img.shields.io/aur/maintainer/plymouth-theme-manjaro-charge)
![AUR votes](https://img.shields.io/aur/votes/plymouth-theme-manjaro-charge)

A Plymouth theme based on Fedora's Charge theme, but featuring the Manjaro logo and colours:

![boot-up animation](previews/boot-up.gif)

With also a support for a different shutdown animation:

![shutdown animation](previews/shutdown.gif)

[[_TOC_]]

## What is plymouth?

[Plymouth](http://www.freedesktop.org/wiki/Software/Plymouth) is a project from Fedora and now listed among the [freedesktop.org's official resources](https://www.freedesktop.org/wiki/Software/#graphicsdriverswindowsystemsandsupportinglibraries) providing a flicker-free graphical boot process. It relies on [kernel mode setting](https://wiki.archlinux.org/index.php/Kernel_mode_setting) (KMS) to set the native resolution of the display as early as possible, then provides an eye-candy splash screen leading all the way up to the login manager.

### How to set it up?

follow [this](https://wiki.archlinux.org/index.php/plymouth) *archwiki* article to setup plymouth in *archlinux* or any other distro.

## Installation

### How to get it?

**AUR :** On `manjaro` (or `archlinux`), you can install this theme with an `AUR helper like trizen`
```sh
trizen -S plymouth-theme-manjaro-charge
```

**Download :** you can download individual themes via this link
<p align="center">
    <a href="https://gitlab.com/Tutul/plymouth-theme-manjaro-charge/-/archive/main/plymouth-theme-manjaro-charge-main.zip">
        <img alt="download" src="https://img.shields.io/badge/Download-Here-sucess?style=for-the-badge&logo=gitlab">
    </a>
</p>

**Clone :** or you can clone this repository if you want - 
```sh
git clone https://gitlab.com/Tutul/plymouth-theme-manjaro-charge.git
```

> If you didn't use the AUR, you then need to copy the theme folder `manjaro-charge` to: /usr/share/plymouth/themes/

### How to use it?

Those explanation are for manjaro (obviously) and other archlinux-based distro.

The available themes can be listed with:
```sh
plymouth-set-default-theme --list
```

Update your plymouth configuration (as root):
```sh
plymouth-set-default-theme manjaro-charge
```

You need to regenerate your initramfs for it to displayed during the boot process (as root):
```sh
mkinitcpio -P
```
> if you use an other tool than mkinitcpio, like dracut, you need to use the appropriate command.

### Configuration

This theme come with watemark for all Manjaro edition (as per https://manjaro.org/download/).
To enable it, you simply have to replace the symlink pointing on the default watermak to the one you want to choose (as root):
```sh
ln -sf watermark-<EDITION>.png watermark.png
```

> Don't forget to regenerate your initramfs after that to make the change apparent during the boot process (see above).

#### Supported Manjaro's editions

- generic (default, without any logo)

![generic watermark](previews/watermark-default.png)

- Gnome desktop (from plymouth-theme-manjaro-gnome-17.0)

![gnome desktop watermark](previews/watermark-gnome.png)

- Plasma desktop (KDE)

![plasma desktop watermark](previews/watermark-plasma.png)

- Xfce desktop

![xfce desktop watermark](previews/watermark-xfce.png)

- MATE desktop

![mate desktop watermark](previews/watermark-mate.png)

- Sway windows manager

![sway system manager watermark](previews/watermark-sway.png)

- Budgie desktop

![budgie desktop watermark](previews/watermark-budgie.png)

- Cinnamon desktop

![cinnamon desktop watermark](previews/watermark-cinnamon.png)

- i3 windows manager

![i3 system manager watermark](previews/watermark-i3.png)

## CREDITS

Manjaro® name and logo are registered trademarks of Manjaro GmbH & Co KG Some rights reserved. The Manjaro Community is licensed to use the trademark with given terms.

GNOME is licensed under the GNOME Foundation’s License Agreement.

KDE® and the K Desktop Environment® logo are registered trademarks of KDE e.V.

Budgie is protected by the copyright © 2014-2021 Budgie Desktop Developers.

Cinnamon is distributed under the terms of the GNU General Public License, version 2 or later. See the [COPYING file](https://github.com/linuxmint/cinnamon/blob/master/COPYING) for details.

Xfce is the fruit of the work done by the [Xfce Development Team](https://xfce.org/about/credits).

MATE is the fruit of the work done by the [MATE Team](https://mate-desktop.org/team/).

Sway is under the MIT License.

i3 is under the BSD-3-Clause.
